
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class JiraLoginPage {
	
	@FindBy(id="username")
	private WebElement jiraUsername;
	
	@FindBy(id="password")
	private WebElement jiraPassword;
	
	@FindBy(id="login-submit")
	private WebElement loginBtn;
	
	
	public boolean logintojira(String username, String password){
		
		try{
			jiraUsername.clear();
			jiraUsername.sendKeys(username);
			
			jiraPassword.clear();
			jiraPassword.sendKeys(password);
			
			loginBtn.click();
			
			Driver.driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
			
		}catch(Exception e){
			return false;
		}
		return true;
	}

}
