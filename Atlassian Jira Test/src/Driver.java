import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;


public class Driver {
	
	public static WebDriver driver;
	public static WebDriver browser (String browser){
		try{
			if(browser.equalsIgnoreCase("firefox")){
				driver = new FirefoxDriver();
			}else if(browser.equalsIgnoreCase("IE")){
				driver = new InternetExplorerDriver();
			}else if(browser.equalsIgnoreCase("Chrome")){
				driver = new ChromeDriver();
			}	
		
		}catch(Exception e){
				return null;
			}
			return driver;
		}
	
}

