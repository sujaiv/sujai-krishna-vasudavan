import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.*;
import org.openqa.selenium.support.ui.Select;

public class CreateIssue {

	public static void main(String[] args) {
		
		Driver.driver = Driver.browser("firefox");
		Driver.driver.get("https://jira.atlassian.com/browse/TST");
		Driver.driver.findElement(By.linkText("Log In")).click();
		
		JiraLoginPage jiralogin = PageFactory.initElements(Driver.driver, JiraLoginPage.class);
		jiralogin.logintojira("sujaikrishna.v@gmail.com", "Vggsa123!");
		
		Driver.driver.findElement(By.id("create_link")).click();
		Driver.driver.findElement(By.id("summary")).clear();
		Driver.driver.findElement(By.id("summary")).sendKeys("Testing of create new issue - 2");
		Driver.driver.findElement(By.xpath("//*[@id='priority-single-select']/span")).click();
		Driver.driver.findElement(By.linkText("Major")).click();
		Driver.driver.findElement(By.id("environment")).clear();
		Driver.driver.findElement(By.id("environment")).sendKeys("QA");
		Driver.driver.findElement(By.id("description")).clear();
		Driver.driver.findElement(By.id("description")).sendKeys("Test creating new issue in Jira using chrome");
		Driver.driver.findElement(By.xpath("//*[@id='create-issue-submit']")).click();
	}
		
		
}

